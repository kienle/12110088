﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace final.Models
{
    public class PPDieuTri_Login
    {
        public int ID { get; set; }
        public String PhuongPhapDieuTri { get; set; }
        public String CheDoDinhDuong { get; set; }

        public int UserProfileUserId { get; set; }
        public virtual UserProfile UserProfile { get; set; }
    }
}