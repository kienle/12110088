﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace final.Models
{
    public class ThongTinBenh
    {
        public int ID { get; set; }
        public String TuaDe { get; set; }
        public String NoiDung { get; set; }
        public String HinhAnh { get; set; }
    }
}