﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace final.Models
{
    public class BinhLuan
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "Bắt buộc có dữ liệu")]
        public String NoiDung { get; set; }
        [Required(ErrorMessage="Bắt buộc có dữ liệu")]
        public String NguoiGui { get; set; }
        public DateTime NgayTao { get; set; }

        public int HoiDapID { get; set; }
        public virtual HoiDap HoiDap { get; set; }
    }
}