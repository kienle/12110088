﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace final.Models
{
    public class ChiaSe
    {
        public int ID { get; set; }
        [Required(ErrorMessage="Bắt buộc có dữ liệu")]
        public String TuaDe { get; set; }
        [Required(ErrorMessage = "Bắt buộc có dữ liệu")]
        public String NoiDung { get; set; }
        public DateTime NgayTao { get; set; }
        public String NguoiGui { get; set; }

        public int UserProfileUserId { get; set; }
        public virtual UserProfile UserProfile { get; set; }
    }
}