﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace final.Models
{
    public class TinNhanGui
    {
        public int ID { get; set; }
        [Required(ErrorMessage="Bắt buộc có dữ liệu")]
        public String NoiDung { get; set; }
        [Required(ErrorMessage = "Bắt buộc có dữ liệu")]
        public String NguoiNhan { get; set; }

        public int UserProfileUserId { get; set; }
        public virtual UserProfile UserProfile { get; set; }

    }
}