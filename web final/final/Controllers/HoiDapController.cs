﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using final.Models;

namespace final.Controllers
{
    public class HoiDapController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /HoiDap/

        public ActionResult Index()
        {
            var hoidaps = db.HoiDaps.Include(h => h.UserProfile);
            return View(hoidaps.ToList());
        }
        public ActionResult ChiTiet()
        {           
            return PartialView(db.HoiDaps.ToList());
        }

        //
        // GET: /HoiDap/Details/5

        public ActionResult Details(int id = 0)
        {
            HoiDap hoidap = db.HoiDaps.Find(id);
            ViewData["idpost"] = id;
            if (hoidap == null)
            {
                return HttpNotFound();
            }
            return View(hoidap);
        }

        //
        // GET: /HoiDap/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /HoiDap/Create
        [Authorize]
        [HttpPost]
        public ActionResult Create(HoiDap hoidap)
        {
            if (ModelState.IsValid)
            {
                hoidap.NgayTao = DateTime.Now;

                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).
                    Where(y => y.UserName == User.Identity.Name).Single().UserId;
                hoidap.UserProfileUserId = userid;

                hoidap.NguoiGui = db.UserProfiles.Select(x => new { x.UserName }).
                    Where(y => y.UserName == User.Identity.Name).Single().UserName;

                hoidap.NoiDung = "";

                db.HoiDaps.Add(hoidap);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", hoidap.UserProfileUserId);
            return View(hoidap);
        }

        //
        // GET: /HoiDap/Edit/5

        public ActionResult Edit(int id = 0)
        {
            HoiDap hoidap = db.HoiDaps.Find(id);
            if (hoidap == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", hoidap.UserProfileUserId);
            return View(hoidap);
        }

        //
        // POST: /HoiDap/Edit/5

        [HttpPost]
        public ActionResult Edit(HoiDap hoidap)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hoidap).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", hoidap.UserProfileUserId);
            return View(hoidap);
        }

        //
        // GET: /HoiDap/Delete/5

        public ActionResult Delete(int id = 0)
        {
            HoiDap hoidap = db.HoiDaps.Find(id);
            if (hoidap == null)
            {
                return HttpNotFound();
            }
            return View(hoidap);
        }

        //
        // POST: /HoiDap/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            HoiDap hoidap = db.HoiDaps.Find(id);
            db.HoiDaps.Remove(hoidap);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}