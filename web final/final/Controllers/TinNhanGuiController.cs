﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using final.Models;

namespace final.Controllers
{
    public class TinNhanGuiController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /TinNhanGui/

        public ActionResult Index()
        {
            var tinnhanguis = db.TinNhanGuis.Include(t => t.UserProfile);
            return View(tinnhanguis.ToList());
        }

        //
        // GET: /TinNhanGui/Details/5

        public ActionResult Details(int id = 0)
        {
            TinNhanGui tinnhangui = db.TinNhanGuis.Find(id);
            if (tinnhangui == null)
            {
                return HttpNotFound();
            }
            return View(tinnhangui);
        }

        //
        // GET: /TinNhanGui/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /TinNhanGui/Create
        [Authorize]
        [HttpPost]
        public ActionResult Create(TinNhanGui tinnhangui)
        {
            if (ModelState.IsValid)
            {
                tinnhangui.UserProfileUserId = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).
                    Where(y => y.UserName == User.Identity.Name).Single().UserId;

                db.TinNhanGuis.Add(tinnhangui);
                db.SaveChanges();
                return RedirectToAction("Create","TinNhanGui");
            }

            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", tinnhangui.UserProfileUserId);
            return View(tinnhangui);
        }

        //
        // GET: /TinNhanGui/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TinNhanGui tinnhangui = db.TinNhanGuis.Find(id);
            if (tinnhangui == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", tinnhangui.UserProfileUserId);
            return View(tinnhangui);
        }

        //
        // POST: /TinNhanGui/Edit/5

        [HttpPost]
        public ActionResult Edit(TinNhanGui tinnhangui)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tinnhangui).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", tinnhangui.UserProfileUserId);
            return View(tinnhangui);
        }

        //
        // GET: /TinNhanGui/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TinNhanGui tinnhangui = db.TinNhanGuis.Find(id);
            if (tinnhangui == null)
            {
                return HttpNotFound();
            }
            return View(tinnhangui);
        }

        //
        // POST: /TinNhanGui/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            TinNhanGui tinnhangui = db.TinNhanGuis.Find(id);
            db.TinNhanGuis.Remove(tinnhangui);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}