﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using final.Models;

namespace final.Controllers
{
    public class PhuongPhapDieuTriController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /PhuongPhapDieuTri/

        public ActionResult Index()
        {
            return View(db.PPDieuTris.ToList());
        }

        //
        // GET: /PhuongPhapDieuTri/Details/5

        public ActionResult Details(int id = 0)
        {
            PPDieuTri ppdieutri = db.PPDieuTris.Find(id);
            if (ppdieutri == null)
            {
                return HttpNotFound();
            }
            return View(ppdieutri);
        }

        //
        // GET: /PhuongPhapDieuTri/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /PhuongPhapDieuTri/Create

        [HttpPost]
        public ActionResult Create(PPDieuTri ppdieutri)
        {
            if (ModelState.IsValid)
            {
                db.PPDieuTris.Add(ppdieutri);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(ppdieutri);
        }

        //
        // GET: /PhuongPhapDieuTri/Edit/5

        public ActionResult Edit(int id = 0)
        {
            PPDieuTri ppdieutri = db.PPDieuTris.Find(id);
            if (ppdieutri == null)
            {
                return HttpNotFound();
            }
            return View(ppdieutri);
        }

        //
        // POST: /PhuongPhapDieuTri/Edit/5

        [HttpPost]
        public ActionResult Edit(PPDieuTri ppdieutri)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ppdieutri).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ppdieutri);
        }

        //
        // GET: /PhuongPhapDieuTri/Delete/5

        public ActionResult Delete(int id = 0)
        {
            PPDieuTri ppdieutri = db.PPDieuTris.Find(id);
            if (ppdieutri == null)
            {
                return HttpNotFound();
            }
            return View(ppdieutri);
        }

        //
        // POST: /PhuongPhapDieuTri/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            PPDieuTri ppdieutri = db.PPDieuTris.Find(id);
            db.PPDieuTris.Remove(ppdieutri);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}