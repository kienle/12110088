﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using final.Models;

namespace final.Controllers
{
    public class BinhLuanController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /BinhLuan/

        public ActionResult Index()
        {
            var binhluans = db.BinhLuans.Include(b => b.HoiDap);
            return View(binhluans.ToList());
        }

        //
        // GET: /BinhLuan/Details/5

        public ActionResult Details(int id = 0)
        {
            BinhLuan binhluan = db.BinhLuans.Find(id);
            if (binhluan == null)
            {
                return HttpNotFound();
            }
            return View(binhluan);
        }

        //
        // GET: /BinhLuan/Create

        public ActionResult Create()
        {
            ViewBag.HoiDapID = new SelectList(db.HoiDaps, "ID", "TuaDe");
            return View();
        }

        //
        // POST: /BinhLuan/Create

        [HttpPost]
        public ActionResult Create(BinhLuan binhluan, int idpost)
        {
            if (ModelState.IsValid)
            {
                binhluan.HoiDapID = idpost;

                binhluan.NgayTao = DateTime.Now;

                db.BinhLuans.Add(binhluan);
                db.SaveChanges();
                return PartialView("xemBinhLuan",binhluan);
                //return RedirectToAction("Details/"+ idpost,"HoiDap");
            }

            ViewBag.HoiDapID = new SelectList(db.HoiDaps, "ID", "TuaDe", binhluan.HoiDapID);
            return View(binhluan);
        }

        //
        // GET: /BinhLuan/Edit/5

        public ActionResult Edit(int id = 0)
        {
            BinhLuan binhluan = db.BinhLuans.Find(id);
            if (binhluan == null)
            {
                return HttpNotFound();
            }
            ViewBag.HoiDapID = new SelectList(db.HoiDaps, "ID", "TuaDe", binhluan.HoiDapID);
            return View(binhluan);
        }

        //
        // POST: /BinhLuan/Edit/5

        [HttpPost]
        public ActionResult Edit(BinhLuan binhluan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(binhluan).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.HoiDapID = new SelectList(db.HoiDaps, "ID", "TuaDe", binhluan.HoiDapID);
            return View(binhluan);
        }

        //
        // GET: /BinhLuan/Delete/5

        public ActionResult Delete(int id = 0)
        {
            BinhLuan binhluan = db.BinhLuans.Find(id);
            if (binhluan == null)
            {
                return HttpNotFound();
            }
            return View(binhluan);
        }

        //
        // POST: /BinhLuan/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            BinhLuan binhluan = db.BinhLuans.Find(id);
            db.BinhLuans.Remove(binhluan);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}