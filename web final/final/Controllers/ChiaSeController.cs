﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using final.Models;

namespace final.Controllers
{
    public class ChiaSeController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /ChiaSe/

        public ActionResult Index()
        {
            var chiases = db.ChiaSes.Include(c => c.UserProfile);
            return View(chiases.ToList());
        }

        public ActionResult ChiTiet()
        {
            return PartialView(db.ChiaSes.ToList());
        }

        //
        // GET: /ChiaSe/Details/5

        public ActionResult Details(int id = 0)
        {
            ChiaSe chiase = db.ChiaSes.Find(id);
            if (chiase == null)
            {
                return HttpNotFound();
            }
            return View(chiase);
        }

        //
        // GET: /ChiaSe/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /ChiaSe/Create
        [Authorize]
        [HttpPost]
        public ActionResult Create(ChiaSe chiase)
        {
            if (ModelState.IsValid)
            {
                chiase.NgayTao = DateTime.Now;

                int idchiase = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).
                    Where(y => y.UserName == User.Identity.Name).Single().UserId;
                chiase.UserProfileUserId = idchiase;

                chiase.NguoiGui = db.UserProfiles.Select(x => new { x.UserName }).
                    Where(y => y.UserName == User.Identity.Name).Single().UserName;

                db.ChiaSes.Add(chiase);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", chiase.UserProfileUserId);
            return View(chiase);
        }

        //
        // GET: /ChiaSe/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ChiaSe chiase = db.ChiaSes.Find(id);
            if (chiase == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", chiase.UserProfileUserId);
            return View(chiase);
        }

        //
        // POST: /ChiaSe/Edit/5

        [HttpPost]
        public ActionResult Edit(ChiaSe chiase)
        {
            if (ModelState.IsValid)
            {
                db.Entry(chiase).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", chiase.UserProfileUserId);
            return View(chiase);
        }

        //
        // GET: /ChiaSe/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ChiaSe chiase = db.ChiaSes.Find(id);
            if (chiase == null)
            {
                return HttpNotFound();
            }
            return View(chiase);
        }

        //
        // POST: /ChiaSe/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            ChiaSe chiase = db.ChiaSes.Find(id);
            db.ChiaSes.Remove(chiase);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}