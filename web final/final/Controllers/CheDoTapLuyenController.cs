﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using final.Models;

namespace final.Controllers
{
    public class CheDoTapLuyenController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /CheDoTapLuyen/

        public ActionResult Index()
        {
            return View(db.CheDoTapLuyens.ToList());
        }

        //
        // GET: /CheDoTapLuyen/Details/5

        public ActionResult Details(int id = 0)
        {
            CheDoTapLuyen chedotapluyen = db.CheDoTapLuyens.Find(id);
            if (chedotapluyen == null)
            {
                return HttpNotFound();
            }
            return View(chedotapluyen);
        }

        //
        // GET: /CheDoTapLuyen/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /CheDoTapLuyen/Create

        [HttpPost]
        public ActionResult Create(CheDoTapLuyen chedotapluyen)
        {
            if (ModelState.IsValid)
            {
                db.CheDoTapLuyens.Add(chedotapluyen);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(chedotapluyen);
        }

        //
        // GET: /CheDoTapLuyen/Edit/5

        public ActionResult Edit(int id = 0)
        {
            CheDoTapLuyen chedotapluyen = db.CheDoTapLuyens.Find(id);
            if (chedotapluyen == null)
            {
                return HttpNotFound();
            }
            return View(chedotapluyen);
        }

        //
        // POST: /CheDoTapLuyen/Edit/5

        [HttpPost]
        public ActionResult Edit(CheDoTapLuyen chedotapluyen)
        {
            if (ModelState.IsValid)
            {
                db.Entry(chedotapluyen).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(chedotapluyen);
        }

        //
        // GET: /CheDoTapLuyen/Delete/5

        public ActionResult Delete(int id = 0)
        {
            CheDoTapLuyen chedotapluyen = db.CheDoTapLuyens.Find(id);
            if (chedotapluyen == null)
            {
                return HttpNotFound();
            }
            return View(chedotapluyen);
        }

        //
        // POST: /CheDoTapLuyen/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            CheDoTapLuyen chedotapluyen = db.CheDoTapLuyens.Find(id);
            db.CheDoTapLuyens.Remove(chedotapluyen);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}