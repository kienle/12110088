﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using final.Models;

namespace final.Controllers
{
    public class ThongTinBenhController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /ThongTinBenh/

        public ActionResult Index()
        {
            return View(db.ThongTinBenhs.ToList());
        }

        //
        // GET: /ThongTinBenh/Details/5

        public ActionResult Details(int id = 0)
        {
            ThongTinBenh thongtinbenh = db.ThongTinBenhs.Find(id);
            if (thongtinbenh == null)
            {
                return HttpNotFound();
            }
            return View(thongtinbenh);
        }

        //
        // GET: /ThongTinBenh/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /ThongTinBenh/Create

        [HttpPost]
        public ActionResult Create(ThongTinBenh thongtinbenh)
        {
            if (ModelState.IsValid)
            {
                db.ThongTinBenhs.Add(thongtinbenh);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(thongtinbenh);
        }

        //
        // GET: /ThongTinBenh/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ThongTinBenh thongtinbenh = db.ThongTinBenhs.Find(id);
            if (thongtinbenh == null)
            {
                return HttpNotFound();
            }
            return View(thongtinbenh);
        }

        //
        // POST: /ThongTinBenh/Edit/5

        [HttpPost]
        public ActionResult Edit(ThongTinBenh thongtinbenh)
        {
            if (ModelState.IsValid)
            {
                db.Entry(thongtinbenh).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(thongtinbenh);
        }

        //
        // GET: /ThongTinBenh/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ThongTinBenh thongtinbenh = db.ThongTinBenhs.Find(id);
            if (thongtinbenh == null)
            {
                return HttpNotFound();
            }
            return View(thongtinbenh);
        }

        //
        // POST: /ThongTinBenh/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            ThongTinBenh thongtinbenh = db.ThongTinBenhs.Find(id);
            db.ThongTinBenhs.Remove(thongtinbenh);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}