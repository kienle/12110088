﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using final.Models;

namespace final.Controllers
{
    public class TinNoiBatController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /TinNoiBat/

        public ActionResult Index()
        {
            return View(db.TinNoiBats.ToList());
        }

        //
        // GET: /TinNoiBat/Details/5

        public ActionResult Details(int id = 0)
        {
            TinNoiBat tinnoibat = db.TinNoiBats.Find(id);
            if (tinnoibat == null)
            {
                return HttpNotFound();
            }
            return View(tinnoibat);
        }

        //
        // GET: /TinNoiBat/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /TinNoiBat/Create

        [HttpPost]
        public ActionResult Create(TinNoiBat tinnoibat)
        {
            if (ModelState.IsValid)
            {
                db.TinNoiBats.Add(tinnoibat);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tinnoibat);
        }

        //
        // GET: /TinNoiBat/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TinNoiBat tinnoibat = db.TinNoiBats.Find(id);
            if (tinnoibat == null)
            {
                return HttpNotFound();
            }
            return View(tinnoibat);
        }

        //
        // POST: /TinNoiBat/Edit/5

        [HttpPost]
        public ActionResult Edit(TinNoiBat tinnoibat)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tinnoibat).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tinnoibat);
        }

        //
        // GET: /TinNoiBat/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TinNoiBat tinnoibat = db.TinNoiBats.Find(id);
            if (tinnoibat == null)
            {
                return HttpNotFound();
            }
            return View(tinnoibat);
        }

        //
        // POST: /TinNoiBat/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            TinNoiBat tinnoibat = db.TinNoiBats.Find(id);
            db.TinNoiBats.Remove(tinnoibat);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}