﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using final.Models;

namespace final.Controllers
{
    [Authorize]
    public class PhuongPhapDieuTriLoginController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /PhuongPhapDieuTriLogin/

        public ActionResult Index()
        {
            var ppdieutri_logins = db.PPDieuTri_Logins.Include(p => p.UserProfile);
            return View(ppdieutri_logins.ToList());
        }

        //
        // GET: /PhuongPhapDieuTriLogin/Details/5

        public ActionResult Details(int id = 0)
        {
            PPDieuTri_Login ppdieutri_login = db.PPDieuTri_Logins.Find(id);
            if (ppdieutri_login == null)
            {
                return HttpNotFound();
            }
            return View(ppdieutri_login);
        }

        //
        // GET: /PhuongPhapDieuTriLogin/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /PhuongPhapDieuTriLogin/Create

        [HttpPost]
        public ActionResult Create(PPDieuTri_Login ppdieutri_login)
        {
            if (ModelState.IsValid)
            {
                ppdieutri_login.UserProfileUserId = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).
                  Where(y => y.UserName == User.Identity.Name).Single().UserId;
                

                db.PPDieuTri_Logins.Add(ppdieutri_login);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", ppdieutri_login.UserProfileUserId);
            return View(ppdieutri_login);
        }

        //
        // GET: /PhuongPhapDieuTriLogin/Edit/5

        public ActionResult Edit(int id = 0)
        {
            PPDieuTri_Login ppdieutri_login = db.PPDieuTri_Logins.Find(id);
            if (ppdieutri_login == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", ppdieutri_login.UserProfileUserId);
            return View(ppdieutri_login);
        }

        //
        // POST: /PhuongPhapDieuTriLogin/Edit/5

        [HttpPost]
        public ActionResult Edit(PPDieuTri_Login ppdieutri_login)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ppdieutri_login).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", ppdieutri_login.UserProfileUserId);
            return View(ppdieutri_login);
        }

        //
        // GET: /PhuongPhapDieuTriLogin/Delete/5

        public ActionResult Delete(int id = 0)
        {
            PPDieuTri_Login ppdieutri_login = db.PPDieuTri_Logins.Find(id);
            if (ppdieutri_login == null)
            {
                return HttpNotFound();
            }
            return View(ppdieutri_login);
        }

        //
        // POST: /PhuongPhapDieuTriLogin/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            PPDieuTri_Login ppdieutri_login = db.PPDieuTri_Logins.Find(id);
            db.PPDieuTri_Logins.Remove(ppdieutri_login);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}