namespace final.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TinNhanNhans", "TinNhanGuiID", "dbo.TinNhanGuis");
            DropIndex("dbo.TinNhanNhans", new[] { "TinNhanGuiID" });
            DropTable("dbo.TinNhanNhans");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.TinNhanNhans",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NoiDung = c.String(),
                        TinNhanGuiID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateIndex("dbo.TinNhanNhans", "TinNhanGuiID");
            AddForeignKey("dbo.TinNhanNhans", "TinNhanGuiID", "dbo.TinNhanGuis", "ID", cascadeDelete: true);
        }
    }
}
