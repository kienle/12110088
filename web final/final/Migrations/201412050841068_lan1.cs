namespace final.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.HoiDaps",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TuaDe = c.String(),
                        NoiDung = c.String(),
                        NgayTao = c.DateTime(nullable: false),
                        NguoiGui = c.String(),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: true)
                .Index(t => t.UserProfileUserId);
            
            CreateTable(
                "dbo.BinhLuans",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NoiDung = c.String(),
                        NguoiGui = c.String(),
                        NgayTao = c.DateTime(nullable: false),
                        HoiDapID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.HoiDaps", t => t.HoiDapID, cascadeDelete: true)
                .Index(t => t.HoiDapID);
            
            CreateTable(
                "dbo.ChiaSes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TuaDe = c.String(),
                        NoiDung = c.String(),
                        NgayTao = c.DateTime(nullable: false),
                        NguoiGui = c.String(),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: true)
                .Index(t => t.UserProfileUserId);
            
            CreateTable(
                "dbo.TinNhanGuis",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NoiDung = c.String(),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: true)
                .Index(t => t.UserProfileUserId);
            
            CreateTable(
                "dbo.TinNhanNhans",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NoiDung = c.String(),
                        NguoiGui = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ThongTinBenhs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TuaDe = c.String(),
                        NoiDung = c.String(),
                        HinhAnh = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PPDieuTris",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TuaDe = c.String(),
                        NoiDung = c.String(),
                        HinhAnh = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PPDieuTri_Login",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PhuongPhapDieuTri = c.String(),
                        CheDoDinhDuong = c.String(),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: true)
                .Index(t => t.UserProfileUserId);
            
            CreateTable(
                "dbo.CheDoTapLuyens",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TuaDe = c.String(),
                        NoiDung = c.String(),
                        HinhAnh = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TinNoiBats",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TuaDe = c.String(),
                        NoiDung = c.String(),
                        HinhAnh = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Gui_Nhan",
                c => new
                    {
                        TinNhanGuiID = c.Int(nullable: false),
                        TinNhanNhanID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TinNhanGuiID, t.TinNhanNhanID })
                .ForeignKey("dbo.TinNhanGuis", t => t.TinNhanGuiID, cascadeDelete: true)
                .ForeignKey("dbo.TinNhanNhans", t => t.TinNhanNhanID, cascadeDelete: true)
                .Index(t => t.TinNhanGuiID)
                .Index(t => t.TinNhanNhanID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Gui_Nhan", new[] { "TinNhanNhanID" });
            DropIndex("dbo.Gui_Nhan", new[] { "TinNhanGuiID" });
            DropIndex("dbo.PPDieuTri_Login", new[] { "UserProfileUserId" });
            DropIndex("dbo.TinNhanGuis", new[] { "UserProfileUserId" });
            DropIndex("dbo.ChiaSes", new[] { "UserProfileUserId" });
            DropIndex("dbo.BinhLuans", new[] { "HoiDapID" });
            DropIndex("dbo.HoiDaps", new[] { "UserProfileUserId" });
            DropForeignKey("dbo.Gui_Nhan", "TinNhanNhanID", "dbo.TinNhanNhans");
            DropForeignKey("dbo.Gui_Nhan", "TinNhanGuiID", "dbo.TinNhanGuis");
            DropForeignKey("dbo.PPDieuTri_Login", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.TinNhanGuis", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.ChiaSes", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.BinhLuans", "HoiDapID", "dbo.HoiDaps");
            DropForeignKey("dbo.HoiDaps", "UserProfileUserId", "dbo.UserProfile");
            DropTable("dbo.Gui_Nhan");
            DropTable("dbo.TinNoiBats");
            DropTable("dbo.CheDoTapLuyens");
            DropTable("dbo.PPDieuTri_Login");
            DropTable("dbo.PPDieuTris");
            DropTable("dbo.ThongTinBenhs");
            DropTable("dbo.TinNhanNhans");
            DropTable("dbo.TinNhanGuis");
            DropTable("dbo.ChiaSes");
            DropTable("dbo.BinhLuans");
            DropTable("dbo.HoiDaps");
            DropTable("dbo.UserProfile");
        }
    }
}
