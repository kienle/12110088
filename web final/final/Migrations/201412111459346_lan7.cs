namespace final.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan7 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.HoiDaps", "NoiDung", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.HoiDaps", "NoiDung", c => c.String(nullable: false));
        }
    }
}
