// <auto-generated />
namespace final.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class lan7 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(lan7));
        
        string IMigrationMetadata.Id
        {
            get { return "201412111459346_lan7"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
