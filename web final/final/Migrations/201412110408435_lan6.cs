namespace final.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan6 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BinhLuans", "NoiDung", c => c.String(nullable: false));
            AlterColumn("dbo.BinhLuans", "NguoiGui", c => c.String(nullable: false));
            AlterColumn("dbo.TinNhanGuis", "NoiDung", c => c.String(nullable: false));
            AlterColumn("dbo.TinNhanGuis", "NguoiNhan", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TinNhanGuis", "NguoiNhan", c => c.String());
            AlterColumn("dbo.TinNhanGuis", "NoiDung", c => c.String());
            AlterColumn("dbo.BinhLuans", "NguoiGui", c => c.String());
            AlterColumn("dbo.BinhLuans", "NoiDung", c => c.String());
        }
    }
}
