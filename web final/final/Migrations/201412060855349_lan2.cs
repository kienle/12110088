namespace final.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Gui_Nhan", "TinNhanGuiID", "dbo.TinNhanGuis");
            DropForeignKey("dbo.Gui_Nhan", "TinNhanNhanID", "dbo.TinNhanNhans");
            DropIndex("dbo.Gui_Nhan", new[] { "TinNhanGuiID" });
            DropIndex("dbo.Gui_Nhan", new[] { "TinNhanNhanID" });
            AddColumn("dbo.TinNhanGuis", "NguoiNhan", c => c.String());
            AddColumn("dbo.TinNhanNhans", "TinNhanGuiID", c => c.Int(nullable: false));
            AddForeignKey("dbo.TinNhanNhans", "TinNhanGuiID", "dbo.TinNhanGuis", "ID", cascadeDelete: true);
            CreateIndex("dbo.TinNhanNhans", "TinNhanGuiID");
            DropColumn("dbo.TinNhanNhans", "NguoiGui");
            DropTable("dbo.Gui_Nhan");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Gui_Nhan",
                c => new
                    {
                        TinNhanGuiID = c.Int(nullable: false),
                        TinNhanNhanID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TinNhanGuiID, t.TinNhanNhanID });
            
            AddColumn("dbo.TinNhanNhans", "NguoiGui", c => c.String());
            DropIndex("dbo.TinNhanNhans", new[] { "TinNhanGuiID" });
            DropForeignKey("dbo.TinNhanNhans", "TinNhanGuiID", "dbo.TinNhanGuis");
            DropColumn("dbo.TinNhanNhans", "TinNhanGuiID");
            DropColumn("dbo.TinNhanGuis", "NguoiNhan");
            CreateIndex("dbo.Gui_Nhan", "TinNhanNhanID");
            CreateIndex("dbo.Gui_Nhan", "TinNhanGuiID");
            AddForeignKey("dbo.Gui_Nhan", "TinNhanNhanID", "dbo.TinNhanNhans", "ID", cascadeDelete: true);
            AddForeignKey("dbo.Gui_Nhan", "TinNhanGuiID", "dbo.TinNhanGuis", "ID", cascadeDelete: true);
        }
    }
}
