namespace final.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan5 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.HoiDaps", "TuaDe", c => c.String(nullable: false));
            AlterColumn("dbo.HoiDaps", "NoiDung", c => c.String(nullable: false));
            AlterColumn("dbo.ChiaSes", "TuaDe", c => c.String(nullable: false));
            AlterColumn("dbo.ChiaSes", "NoiDung", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ChiaSes", "NoiDung", c => c.String());
            AlterColumn("dbo.ChiaSes", "TuaDe", c => c.String());
            AlterColumn("dbo.HoiDaps", "NoiDung", c => c.String());
            AlterColumn("dbo.HoiDaps", "TuaDe", c => c.String());
        }
    }
}
