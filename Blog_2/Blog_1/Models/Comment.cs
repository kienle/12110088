﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_1.Models
{
    public class Comment
    {
        [Required]
        public int ID { get; set; }
        [Required]
        [MinLength(50, ErrorMessage = "Tối thiểu 50 ký tự")]
        public String Body { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime DateCreated { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime DateUpdated { get; set; }
        [Required]
        public String Author { get; set; }
        [Required]
        public int PostID { get; set; }
        public virtual Post Posts { get; set; }
    }
}