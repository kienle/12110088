﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_1.Models
{
    public class Account
    {
        [Required]
        [DataType(DataType.Password)]
        public int Password { get; set; }
        [Required]
        [RegularExpression("\\D\\w*@(\\w+\\.\\w+)+", ErrorMessage = "Email không hợp lệ")]
        public String Email { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "Tối đa 100 ký tự")]
        public String FirstName { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "Tối đa 100 ký tự")]
        public String LastName { get; set; }
        [Required]
        public int AccountID { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
    }
}