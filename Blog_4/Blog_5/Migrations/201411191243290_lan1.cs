namespace Blog_5.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Body = c.String(nullable: false),
                        DayCreated = c.DateTime(nullable: false),
                        DayUpdated = c.DateTime(nullable: false),
                        Author = c.String(nullable: false),
                        PostID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .Index(t => t.PostID);
            
            AddColumn("dbo.Posts", "DateUpdated", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Posts", "Title", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Posts", "Body", c => c.String(nullable: false));
            AlterColumn("dbo.Tags", "Content", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            DropIndex("dbo.Comments", new[] { "PostID" });
            DropForeignKey("dbo.Comments", "PostID", "dbo.Posts");
            AlterColumn("dbo.Tags", "Content", c => c.String());
            AlterColumn("dbo.Posts", "Body", c => c.String());
            AlterColumn("dbo.Posts", "Title", c => c.String());
            DropColumn("dbo.Posts", "DateUpdated");
            DropTable("dbo.Comments");
        }
    }
}
