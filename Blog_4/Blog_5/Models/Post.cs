﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_5.Models
{
    public class Post
    {
        [Required]
        public int ID { get; set; }
        [Required]
        [StringLength(500, MinimumLength = 20, ErrorMessage = "Tối thiểu 20 ký tự và tối đa 500 ký tự")]
        public String Title { get; set; }
        [Required]
        [MinLength(50, ErrorMessage = "Tối thiểu 50 ký tự")]
        public String Body { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime DateCreated { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime DateUpdated { get; set; }

        public virtual UserProfile UserProfile { get; set; }
        public int UserProfileUserId { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }

    }
}