﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_5.Models
{
    public class Tag
    {
        [Required]
        public int ID { get; set; }
        [Required]
        [StringLength(100, MinimumLength = 10, ErrorMessage = "Tối thiểu 10 ký tự và tối đa 100 ký tự")]
        public String Content { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
    }
}